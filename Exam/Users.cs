﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam
{
    public class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Phonenumber { get; set; }
        public string Password { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}
